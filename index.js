const path = require('path');

module.exports = (api, options) => {
  api.chainWebpack(webpackConfig => {
    webpackConfig.resolve.alias.set(
      'api',
      // TODO: Make this use the options.
      // options.api, <~ does not work :(
      path.join(api.getCwd(), 'db/json-server.js'),
    );
  });
};
