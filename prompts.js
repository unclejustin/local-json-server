module.exports = [
  {
    type: 'confirm',
    name: 'useWhatwgFetch',
    message: 'Use whatwg-fetch polyfill?',
    default: true,
    group: 'Strongly recommended',
    description:
      'Adds the whatwg-fetch polyfill. Needed if you are going to use Cypress for testing.',
  },
  {
    type: 'confirm',
    name: 'addExampleApi',
    message: 'Add example data skeleton, component and api calls?',
    default: true,
    group: 'Quick Start',
    description:
      'Adds some example api calls and data to db.json. Useful if this is your first time :).',
  },
  {
    type: 'input',
    name: 'api',
    message: 'The alias to use for your api call.',
    default: 'api',
    description: 'This is the `module` name you will import to make api calls.',
  },
];
