<% if (options.useWhatwgFetch) { %>
import { fetch as fetchPolyfill } from 'whatwg-fetch';

if (!window.fetch) {
  window.fetch = fetchPolyfill;
}

<% } %>const apiUrl = 'http://localhost:3000';

const fetchWrapper = (url, params) =>
  fetch(url, params).then(response => response.json());

const _get = url => fetchWrapper(url);

const _post = (url, data) => {
  const params = {
    body: JSON.stringify(data),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  return fetchWrapper(url, params);
};

const _patch = (url, data) => {
  const params = {
    body: JSON.stringify(data),
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  return fetchWrapper(url, params);
};

const _delete = url => {
  const params = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  return fetchWrapper(url, params);
};

// Generic api calls for quick development
const getItems = (path) => _get(`${apiUrl}/${path}`);
const getItem = (path, id) => _get(`${apiUrl}/${path}/${id}`);
const addItem = (path, item) => _post(`${apiUrl}/${path}`, item);
const updateItem = (path, id, item) => _patch(`${apiUrl}/${path}/${id}`, item);
const deleteItem = (path, id) => _delete(`${apiUrl}/${path}/${id}`);

<% if (options.addExampleApi) { %>
/**
 * Ideally these map directly to actual remote calls.
 * The idea is that all you have to do is reference a
 * different api file for production.
 */
const getUsers = () => _get(`${apiUrl}/users`);
const getUser = id => _get(`${apiUrl}/users/${id}`);
const addUser = user => _post(`${apiUrl}/users`, user);
const updateUser = user => _patch(`${apiUrl}/users/${user.id}`, user);
const deleteUser = id => _delete(`${apiUrl}/users/${id}`);<% } %>

export default {
  getItems,
  getItem,
  addItem,
  updateItem,
  deleteItem,
  <% if (options.addExampleApi) { %>getUsers,
  getUser,
  addUser,
  updateUser,
  deleteUser,<% } %>
};
