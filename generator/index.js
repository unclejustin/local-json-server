module.exports = (api, options) => {
  const hasCypress = api.hasPlugin('e2e-cypress');

  // Install dependencies
  let dependencies = {
    'json-server': '^0.15.0',
    'npm-run-all': '^4.1.5',
  };

  if (options.useWhatwgFetch) {
    dependencies['whatwg-fetch'] = '^3.0.0';
  }

  const scripts = {
    'db:dev': 'json-server db/db.json --watch',
    'db:test': 'json-server db/db.test.json --watch',
    'db:test:reset': 'node db/dbReset.js',
    'dev:serve': 'npm-run-all --parallel db:dev serve',
  };

  if (hasCypress) {
    scripts['test:e2e:db'] =
      'npm-run-all db:test:reset --parallel db:test test:e2e';
  }

  api.extendPackage({ dependencies, scripts });

  // Add api and db.json
  api.render('./template');

  // Add example component
  if (options.addExampleApi) {
    api.render('./example');
  }

  // Add test database if Cypress is installed
  if (hasCypress) {
    api.render('./cypress');
  }
};
